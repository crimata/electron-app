const axios = require('axios').default;

const config = require("./config");


async function post(route, body)
{
    let error;
    let data;

    try
    {
        const res = await axios.post(config.API + route, body);
        data = res.data;
        error = false;
    }
    catch (e)
    {
        if (e.response)
        {
            data = e.response.data;
        }
        else if (e.request)
        {
            data = "Can't connect to the server."
        }
        else
        {
            data = "Unknown error occured."
        }

        error = true;
    }

    return { error, data };
}

exports.post = post;
