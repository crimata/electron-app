const { Notification, ipcMain } = require("electron");

const store = require("./utils/store");
const { backgroundMitt, ipcEmit } = require("./utils/emitter");
const { connectToPlatform, disconnectFromPlatform } = require("./io");
// const { initAudio, terminateAudio, playback, streamState } = require("./audio");

const util = require('util');

let ui;
let current;

const messageQueue = [];

let processContentId;

/* Possible messages:
 *    - New UI (set ui)
 *    - New message (add message to ui.messages)
 *    - New content (add content to existing message)
 *    - Update person
 *    - Notification
 */
backgroundMitt.on("message", (message) => messageQueue.push(message));

function handleMessageQueue()
{
    // if (!streamState.pb && !streamState.rec)
    // {
        const update = messageQueue.shift();

        console.log(update);

        if (update)
        {          
            if (update.person)
            {
                ui = update; 
                current = update.messages.at(-1);
            }
            else if (update.name)
            {
                ui.person = update;
            }
            else if (update.modifier)
            {
                ui.messages.push(update); 
                current = update;
            }
            else if (update.category)
            {
                current.content.push(update);
            }
            else if (update.context)
            {
                current.context = update.context;
            }
            else if (update.text)
            {
                current.content.at(-1).text = update.text;
            }
            else
            {
                const content = current.content.at(-1);

                new Notification({
                    title: current.context, 
                    body: content.text
                }).show();

                /* Ideally, we want a more native solution than this */
                // if (current.modifier == "ai" && content.category == "audio")
                // {
                //     playback(content.blob, current.id);
                // }

                return;
            }

            ipcEmit("message", update);
        }
    // }
}

function launchSession(account)
{
    connectToPlatform(account);
    // initAudio();
    processContentId = setInterval(handleMessageQueue, 100);
}

function endSession()
{
    clearInterval(processContentId);
    // terminateAudio();
    disconnectFromPlatform();
    ui = null;
}

ipcMain.on("messenger", () => {
    if (ui) {
        ipcEmit("message", ui);
    }
});

exports.launchSession = launchSession;
exports.endSession = endSession;
