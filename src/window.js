const path = require("path");
const { app, BrowserWindow, ipcMain } = require("electron");

const store = require("./utils/store");
const { sendMessage } = require("./io");
const { backgroundMitt } = require("./utils/emitter");

let win;

function updateWindowPosition()
{
    const bounds = win.getBounds();
    const position = win.getPosition();

    store.set("window-position", {
        width: bounds.width, 
        height: bounds.height, 
        x: position[0], 
        y: position[1]
    })
}


function onNav(_e, cmd) 
{
    cmd === "close" ? win.close() : win.minimize();
}

function createWin()
{
    if (win) return;

    let pos = store.get("window-position");
    if (!pos) pos = { width: 350, height: 750, x: null, y: null }

    win = new BrowserWindow({
        width:           pos.width,
        height:          pos.height,
        x:               pos.x,
        y:               pos.y,
        resizable:       true,
        frame:           false,
        minWidth:        350,
        minHeight:       500,
        backgroundColor: "#EBEBEB",
        webPreferences:  { preload: path.join(__dirname, "render/preload.js") }
    })

    // TODO: find a simpler way to notify platform of user events
    .on("focus", () => store.set("focus", true))
    .on("blur", () => store.set("focus", false))

    .on("closed", () => {
        store.set("focus", false);
        win = null;
    })

    .on("moved", updateWindowPosition)
    .on("resize", updateWindowPosition);

    win.loadFile(path.join(__dirname, "render/index.html"));

    win.webContents.on("did-finish-load", () => {

        backgroundMitt.removeAllListeners("ipc-renderer");

        backgroundMitt.on("ipc-renderer", (channel, ...args) => {
            if (win) 
            {
                win.webContents.send(channel, ...args);    
            }
        });

        win.webContents.send("account", store.get("account"));
    });
}

ipcMain.on("nav", onNav);

exports.createWin = createWin;
