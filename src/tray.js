const fs = require("fs");
const path = require("path");
const { Tray } = require("electron");

const { backgroundMitt } = require("./utils/emitter");

let tray;
const TRAY_PATH = path.join(__dirname, "assets/tray");

/* Current state of recording, playback, and ws connection */
const state = {
    recording: false,
    playback: false,
    disconnect: 0 // whether we're experiencing connection issues
}

/**
 * Updates the tray icon given a new state of an attribute.
 * @param {"recording" | "playback" | "disconnect"} attribute
 * @param {boolean} newState
 */
function updateTray(attribute, newState) 
{
    state[attribute] = newState;

    // convert boolean to number
    const bi = (b) => b ? 1 : 0;

    // Make sure we have the icon we're looking for.
    tray.setImage(path.join(
        TRAY_PATH, 
        `${bi(state.recording)}${bi(state.playback)}${bi(state.disconnect)}` + 
        ".png"
    ));
}

function initTray()
{
    tray = new Tray(path.join(TRAY_PATH, "000.png"));
}

exports.initTray = initTray;
exports.updateTray = updateTray;
